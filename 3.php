<?php 
    function operacion($numero1,$numero2,$operacion){
        $resultado=0;
        switch ($operacion){
            case 'suma':
                $resultado=$numero1+$numero2;
                break;
            case 'producto':
                $resultado=$numero1*$numero2;
                break;
            default:
                $resultado="operacion no implementada";
        }
        return $resultado;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo operacion(2,5,"suma");
        echo "<br>";
        echo operacion(5,4,"producto");
        echo "<br>";
        echo operacion(33,4,"resta");
        echo "<br>";
        ?>
    </body>
</html>

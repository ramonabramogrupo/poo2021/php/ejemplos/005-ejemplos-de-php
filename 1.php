<?php 
    /**
     * funcion que imprime una lista con los 3 textos pasados
     * @param string $texto1
     * @param string $texto2
     * @param string $texto3
     */
    function crear($texto1,$texto2,$texto3){
        echo "<ul>";
        echo "<li>{$texto1}</li>";
        echo "<li>{$texto2}</li>";
        echo "<li>{$texto3}</li>";
        echo "</ul>";
    }
    
    /**
     * funcion que imprime una lista con los 3 textos pasados
     * @param string $texto1
     * @param string $texto2
     * @param string $texto3
     */
    function crear1($texto1,$texto2,$texto3){
    ?>
         <ul>    
        <li><?= $texto1 ?></li>
        <li><?= $texto2 ?></li>
        <li><?= $texto3 ?></li>
        </ul>
    <?php
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            crear("Lorena","Luis","Carmen");
            crear1("Enero","Febrero","Marzo");
        ?>
        
        
        
        
        
    </body>
</html>

<?php
    function operacion($a,$b,$c){
        $c=$a+$b;
    }
    
    function operacionConRetorno($a,$b){
        $c=$a+$b;
        return $c;
    }
    
    function operacionReferencia($a,$b,&$c){
        $c=$a+$b;
    }
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
    </head>
    <body>
        <?php
            $numero1=20;
            $numero2=10;
            $resultado=0;
            operacion($numero1,$numero2,$resultado);
            echo $resultado;
            $resultado=operacionConRetorno($numero1, $numero2);
            echo $resultado;
            $resultado=0;
            operacionReferencia($numero1, $numero2, $resultado);
            echo $resultado;
            
        ?>
    </body>
</html>
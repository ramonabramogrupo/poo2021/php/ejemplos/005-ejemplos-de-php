<?php 
    /**
     * Funcion que retorna el producto de dos numeros
     * @param float $numero1
     * @param float $numero2
     * @return float producto de los dos numeros
     */
    function operacion($numero1,$numero2){
        $resultado=$numero1*$numero2;
        return $resultado;
        //return $numero1*$numero2;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $salida=operacion(34,5);
        var_dump($salida);
        $salida=operacion(3,5);
        var_dump($salida);
        ?>
    </body>
</html>

<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
        Crear una funcion que me indique el numero de veces que se repite un numero dentro de un array
        Sintaxis de la funcion:
        int contar(array con numeros,numero a buscar);
        Para realizarlo creo un array con numeros:
        $x=[1,2,3,4,1,2,3,4,1,1];
        Buscar las veces que se repite el 1.
        </pre>
        <?php
          function contar($vector,$numero){ 
              $repeticiones=0;
              foreach ($vector as $n) {
                  if($n==$numero){
                      $repeticiones++;
                  }
              }
              return $repeticiones;
          }
          
          function contar1($vector,$numero){
              $repeticiones= array_count_values($vector);
              var_dump($repeticiones);
              return $repeticiones[$numero];
          }
         
          $x=[1,2,3,4,1,2,3,4,1,1];
          
          echo "<br>Numero de veces que se repite un numero en un array<br>";
          $resultado=contar($x,1);
          var_dump($resultado);
          
          echo "<br>Numero de veces que se repite un numero en un array con array_count_values<br>";
          $resultado=contar1($x,2);
          var_dump($resultado);
        ?>
    </body>
</html>

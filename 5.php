<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
        Quiero un array bidimensional que almacene los siguientes datos
        codigo,nombre,edad
        1,ana,45
        2,pedro,35
        3,luisa,19
        volcamos todos los datos con un foreach
        </pre>
        <?php
            $datos=[
              [
                  "codigo" => 1,
                  "nombre" => "ana",
                  "edad" => 45
              ],
              [
                  "codigo" => 2,
                  "nombre" => "pedro",
                  "edad" => 35
              ],
              [
                  "codigo" => 3,
                  "nombre" => "luisa",
                  "edad" => 19
                    
              ]
            ];
            /*
            $datos[0]["codigo"]=1;
            $datos[0]["nombre"]="ana";
            $datos[0]["edad"]=45;
            
            $datos[1]["codigo"]=2;
            $datos[1]["nombre"]="pedro";
            $datos[1]["edad"]=35;
            
            $datos[2]["codigo"]=3;
            $datos[2]["nombre"]="luisa";
            $datos[2]["edad"]=19;
            */
            
            //leer edad de pedro
            //echo $datos[1]["edad"];
            foreach($datos as $registro){
                echo "El codigo es " . $registro["codigo"] . "<br>";
                echo "El nombre es " . $registro["nombre"] . "<br>";
                echo "La edad es " . $registro["edad"] . "<br>";
            }
            
            foreach ($datos as $registro){
                foreach($registro as $campo=>$valor){
                    echo "{$campo}: {$valor}<br>";
                }
            }
            
            
        
        ?>
    </body>
</html>

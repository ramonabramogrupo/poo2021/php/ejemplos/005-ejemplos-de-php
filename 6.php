<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
            Crear un array denominado alumnos con los siguientes campos
            id,nombre,apellidos,nota
            Introducimos los siguientes registros
            1,Ana,Vazquez,9
            2,Jose,Lopez,6
            3,Luisa,Marcano,9
            Quiero que mostreis:
            - todos los registros
            - calcular la nota media
        </pre>
        <?php
        $alumnos = [
            [
                "id" => 1,
                "nombre" => "Ana",
                "apellidos" => "Vazquez",
                "nota" => 9
            ],
            [
                "id" => 2,
                "nombre" => 'Jose',
                "apellido" => 'Lopez',
                "nota" => 6
            ],
            [
                "id" => 3,
                "nombre" => 'Luisa',
                "apellido" => 'Marcano',
                "nota" => 9
            ]
        ];
        
        foreach ($alumnos as $indice=>$registro) {
            foreach($registro as $campo=>$valor){
                echo "{$campo}: {$valor}<br>";
            }
            
        }
        
        //$media=($alumnos[0]["nota"]+$alumnos[1]["nota"]+$alumnos[2]["nota"])/3;
        //echo $media;
        
        $suma=0;
        foreach ($alumnos as $registro) {
            $suma=$suma+$registro["nota"];
        }
        $media=$suma/count($alumnos);
        echo $media;
        
        ?>
    </body>
</html>

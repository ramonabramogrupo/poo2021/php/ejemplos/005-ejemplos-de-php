<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
            Crear una funcion llamada contar
            Esa funcion recibe como argumento un array de caracteres y un caracter a buscar dentro del array
            La funcion debe devolver el numero de veces que sale el caracter en el array.
            La sintaxis de la llamada a la funcion:
            int contar(array caracteres,caracter a buscar)
        </pre>
        
        
        <?php
        $letras=['a','s','a','j'];
        
        $veces=0;
        function contar($caracteres,$caracter){
            $cantidad=0;
            foreach ($caracteres as $letra){
                if($letra==$caracter){
                    $cantidad++;
                }
            }
            return $cantidad;
        }
        
        function contar1($caracteres,$caracter){
            $cantidad=0;
            $repeticiones=array_count_values($caracteres);
            $cantidad= $repeticiones[$caracter];
            return $cantidad;
        }
                
        echo "<br>Contar las veces que sale una letra en una array<br>";
        $veces=contar($letras,'a');
        var_dump($veces);
        
        echo "<br>Contar las veces que sale una letra en una array pero con array_count_values<br>";
        var_dump(contar1($letras,'a'));
        
        
        
        ?>
    </body>
</html>

<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function numero(){
            $resultado=0;
            $resultado= mt_rand(1,10);
            return $resultado;
        }
        
        function numeroReferencia(&$numero1){
            $numero1= mt_rand(1,10);
        }
        
        
        echo "<br>utilizando return<br>";
        $a=0;
        $a=numero();
        var_dump($a);
        
        echo "<br>Pasando argumentos por referencia<br>";
        $b=0;
        numeroReferencia($b);
        var_dump($b);
        ?>

    </body>
</html>
